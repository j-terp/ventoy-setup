import base from "./base.json";
import data from './iso.json';
import select from './select.json';

function menu_alias(dir, alias) {
    return {"dir": dir, "alias": alias};
}

function menu_class(dir, code) {
    return [{"dir": dir, "class": code}, {"parent": dir, "class": code}];
}

function menu_tip(dir, tip) {
    return {"dir": dir, "tip": tip};
}

function path(folders) {
    let x = "";
    folders.forEach(folder => {
        if (folder[0] != "/") {
            x += "/" + folder
        }
        else {
            x += folder
        }
    });
    return x;
}

function parse_image(path_to_image, image) {
    let image_path = path([path_to_image, image.dir])
    base.menu_alias.push((menu_alias(image_path, image.alias)))
    let tmp = menu_class(image_path, image.class)
    tmp.forEach(entry => {
        base.menu_class.push(entry)
    })
    if (Object.hasOwn(image, "tip")) {
        base.menu_tip.push((menu_tip(image_path, image.tip)))
    }
    if (Object.hasOwn(image, "sub")) {
        for (let key in image.sub) {
            parse_image(image_path, image.sub[key])
            receipt.push(key)
        }
    }
}

const file = Bun.file(import.meta.dir + '/ventoy/ventoy.json');

let receipt = []
let miss = []

base.menu_alias = []
base.menu_class = []
base.menu_tip = []

for (let key in data.ISO) {
    if (select.includes(key)) {
        let image = data.ISO[key];
        parse_image(data.dir, image);
        receipt.push(key)
    }
}

select.forEach((key => { if (!receipt.includes(key)) miss.push(key)}))

miss.forEach((missing => { for (let parent in data.ISO) { for (let child in data.ISO[parent].sub) {
        if (missing.includes(child)) {
            let image = data.ISO[parent].sub[child];
            parse_image(data.dir, image);
    }}}}
))

await Bun.write(file, JSON.stringify(base, null, 2));